# Ansible Collection - oasis.ruby_kubernetes

This Ansible Collection installs a helm-chart in your ruby project

### Install Collection

Install this using ansible-galaxy (You must be using ansible version >= 2.10)

```
ansible-galaxy collection install git@gitlab.oit.duke.edu:oasis-application-development/ansible-collections/oasis.ruby_kubernetes.git
```

To upgrade to the latest version, you must remove it from your collections path (in ~/.ansible by default) and reinstall it:
```
rm -rf ~/.ansible/collections/ansible_collections/oasis/
ansible-galaxy collection install git@gitlab.oit.duke.edu:oasis-application-development/ansible-collections/oasis.ruby_kubernetes.git
```

### Use Collection

Create or add to an existing playbook something like the following:

```
  collections:
    - oasis.ruby_kubernetes
  tasks:
    - name: "helm-chart"
      include_role:
        name: helm_chart
```
then run the playbook. It does not require become/sudo.
This will produce a helm chart designed to deploy the default rails application, with sqlite.
You can set the following vars in your playbook to override the default:
databases: list of one or more of sqlite (default), postgres, and/or oracle
needs_redis: either `yes` or `no` (default)

See the examples in example_playbooks.
For now, only postgres will impact the resulting helm-chart templates. Oracle must be run outside of Kubernetes, and sqlite
runs in the same container as the server.

### Testing

You can set the full path to the roles directory in this project to your `ANSIBLE_ROLES_PATH` environment variable
and then run the test for a given role. You will want to be in a different directory than this project directory
to run the test or it will generate files/directories in this project directory.

```
export ANSIBLE_ROLES_PATH="${PWD}"
cd $somedir
ansible-playbook ${ANSIBLE_ROLES_PATH}/roles/local_kubernetes/tests/test.yml
```

You can set databases and needs_ansible variables with [--extra-vars set to a JSON format](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#defining-variables-at-runtime):
```
ansible-playbook -e '{"databases":["postgres"],"needs_redis":"yes"}' ${ANSIBLE_ROLES_PATH}/roles/local_kubernetes/tests/test.yml
```
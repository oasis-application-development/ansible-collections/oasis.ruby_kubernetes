Local Kubernetes Development
###

This document describes local development with a locally installed kubernetes

#### Installing Kubernetes

To install kubernetes, use:
- Mac: [rancher-desktop](https://docs.rancherdesktop.io/installation/)
- Linux: [k3s](https://k3s.io/) with [--docker flag](https://rancher.com/docs/k3s/latest/en/advanced/#using-docker-as-the-container-runtime)

#### kubectl 101

The main way of interacting with kubernetes is using the commandline interface [kubectl](https://kubernetes.io/docs/reference/kubectl/cheatsheet/).

1. Configuration
kubectl allows users to store configuration files with authentication information for different clusters (local and remote).

a. Linux
The developer tools install-local-kubernetes.sh creates `~/.kube/config`. This config works the same in all shell sessions.

b. Mac
when rancher-desktop starts, it creates `~/.kube/config`. This config works the same in all shell sessions.

2. Contexts
kubectl allows users to create multiple contexts to work with against the cluster (or clusters) defined in their KUBECONFIG.
This is useful when working on multiple projects, as you can create individual kubernetes namespaces for each project, and
a new context to use that namespace within your config. Note, this actually modifies the config file (KUBECONFIG or ~/.kube/config).
Configuring kubectl to use a context makes kubectl commands work only with that namespace, unless commandline flags are passed
to use specifif namesppaces. Because your user is still an admin, you can still use commandline flags like `-n`, `-A`, etc. to
see things that are running in other namespaces. When you first create your k3s cluster and copy the config, it has a single
context, called `default`, that works with the default namespace unless you pass kubectl flags
to specify a different namespace, e.g. the following will list only pods in the default namespace:
```
kubectl get pods
```
To list pods in another namespace, you can use the `-n` flag
```
kubectl get pods -n kube-system
```

or use the `-A` flag to list pods across all namespaces
```
kubectl get pods -A
```

Other things that you list in kubernetes are similarly affected by the current-context
```
kubectl get deployments
kubectl get services
kubectl get ingress
```

You can see what your current context is, see what other contexts are available, and use a different context
```
kubectl config current-context
kubectl config get-contexts
kubectl config use other-project-contexts
```
You can make kubectl use the default namespace again using
```
kubectl config use default
```

3. Useful commands

Use the following to see what namespaces are in your cluster
```
kubectl get namespaces
```

A good command to run to see if your local cluster is running is
```
kubectl get nodes
```
You should have a single node (your local machine), with STATUS Ready

#### Restart Kubernetes

If things are not working correctly, or if you have just gotten on the VPN, you may need to restart kubernetes.

a. Mac
```
exit Rancher Desktop
```

b. Linux
```
k3s-killall.sh
sudo service k3s start
```

You may want to ensure the node is ready
```
kubectl get nodes
```

#### Scripts

Found in bin/local-kube:

aliases: aliases to common rails commands run inside the kubernetes pod for the application

deploy.sh:
  - creates a namespace with the name of your project
  - deploys the application to the local kubernetes in the new namespace
  - creates or sets your kubectl current-context to use the new namespace
  - configures /etc/hosts with the expected URL for your locally running application
  - can set up locally hosted gem directories in the server pod (see Local Gem Development below)

decommission.sh:
  - decommissions the application from the local kubernetes
  - sets the kubectl current-context to `default` (can be overridden by setting the DEFAULT_KUBE_CONTEXT environment variable)
  - removes the URL from /etc/hosts

configure_local_development_domain.sh: This is run by deploy to add the expected URL to /etc/hosts

remove_local_development_domain.sh: This is run by decommission.sh to remove URL from /etc/hosts

### Notes
Do not use rails db:drop, since it cannot run while the pod with puma is running. Use rails db:reset instead, which also seeds.

rspec runs in separate Pod. `aliases` contains server_bundle for main server pod, rspec_bundle for rspec pod, and bundle_all to
run bundle in both.

### Useful commands during development

1. Set environment and aliases
```
source local-kube.env
export $(grep -v '#' local-kube.env | cut -d= -f1)
source bin/local-kube/aliases
```

2. build application image
```
docker-compose build server
```

3. Check presence of application image
```
docker images | grep ${CI_PROJECT_NAME}_server
```

4. initialize db
```
rails db:migrate
seed
```
(note, you will want to run rails db:migrate:with_data if you are using data-migrations).

6. restart server if config has changed that will not work with normal hot reloading
```
restart_server
```

7. bundle and restart
```
bundle_all
restart_server
```

8. Reset your db (clears out all data)
```
rails db:reset
```

### Local Gem Development

bin/local-kube/deploy.sh can take an optional, space-separated list of paths to gem directories on your host filesystem.
Pass the full path of each gem directory to bin/local-kube/deploy.sh to mount those directories into the kubernetes container
at /opt/app-root/local_gems. All changes you make to files/directories/git checkouts in these directories are also made inside
the container.

Example 1: mount a single gem dir
```
bin/local-kube/deploy.sh ${HOME}/workspace/duke_rad_assets
```
You will find the gem at /opt/app-root/local_gems/duke_rad_assets inside the container.

Example 2, mount multiple gem directories:
```
bin/local-kube/deploy.sh ${HOME}/workspace/duke_rad_assets ${HOME}/workspace/duke_rad_materialize ${HOME}/workspace/rad_exception_notification
```
You will find the following in your container:
/opt/app-root/local_gems/duke_rad_assets
/opt/app-root/local_gems/duke_rad_materialize
/opt/app-root/local_gems/rad_exception_notification

#!/bin/bash

set_helm_context() {
    if ! [ "$(kubectl config current-context)" == "${CI_PROJECT_NAME}" ]
    then
      kubectl config set-context ${CI_PROJECT_NAME} --namespace=${CI_PROJECT_NAME} --cluster=local --user=user || return 1
      kubectl config use ${CI_PROJECT_NAME} || return 1
    fi
}

deployment_name() {
    echo "${CI_PROJECT_NAME}-local"
}

check_required_environment() {
  local required_env="${1}"

  for reqvar in $required_env
  do
    if [ -z "${!reqvar}" ]
    then
      echo "missing ENVIRONMENT ${reqvar}!" >&2
      return 1
    fi
  done
}

check_required_project_environment() {
  check_required_environment "CI_PROJECT_NAME"
}

decommission_template() {
  helm delete $(deployment_name)
}

decommission_domain() {
  sudo -E bin/local-kube/remove_local_development_domain.sh
}

reset_kubectl_context() {
  kubectl config use ${DEFAULT_KUBE_CONTEXT:-default}
}

run_main() {
  check_required_project_environment || return 1
  set_helm_context || return 1
  decommission_template || return 1
  decommission_domain || return 1
  reset_kubectl_context || return 1
  return
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  if [ ! -f "local-kube.env" ]
  then
    echo "missing local-kube.env" >&2
    exit 1
  fi

  source local-kube.env
  export $(grep -v '#' local-kube.env | cut -d= -f1)

  run_main
  if [ $? -gt 0 ]
  then
    exit 1
  fi
fi

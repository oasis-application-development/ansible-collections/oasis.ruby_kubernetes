#!/bin/bash

if [ "$EUID" -ne 0 ]
then
  echo "this will run with sudo -E" >&2
fi

if [ ! -f "local-kube.env" ]
then
  echo "missing local-kube.env" >&2
  exit 1
fi

source local-kube.env
export $(grep -v '#' local-kube.env | cut -d= -f1)

export domain="${CI_ENVIRONMENT_URL##*/}"
if grep "${domain}" /etc/hosts > /dev/null 2>&1
then
  echo "removing ${domain} entry from /etc/hosts" >&2
  sudo -E su root -c 'sed -i.local.bak "/${domain}/d" /etc/hosts'
  sudo rm /etc/hosts.local.bak
fi

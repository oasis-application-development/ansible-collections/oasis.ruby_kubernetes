#!/bin/bash

is_mac() {
  [ "$(uname -s)" == "Darwin" ]
}

create_namespace() {
    if ! kubectl get namespace ${CI_PROJECT_NAME} > /dev/null 2>&1
    then
        kubectl create namespace ${CI_PROJECT_NAME}
        if [ $? -gt 0 ]
        then
            echo 'could not create namespace!' >&2
            return 1
        fi
    fi
}

set_kubectl_context() {
    create_namespace || return
    if [ "$(kubectl config current-context)" != "${CI_PROJECT_NAME}" ]
    then
      if kubectl config get-contexts | grep "${CI_PROJECT_NAME}" > /dev/null 2>&1
      then
         kubectl config use ${CI_PROJECT_NAME} || return 1
      else
        echo "creating kubectl context ${CI_PROJECT_NAME}" >&2
        cluster=$(kubectl config get-clusters | tail -1)
        user=$(kubectl config get-users | tail -1)
        kubectl config set-context ${CI_PROJECT_NAME} --namespace=${CI_PROJECT_NAME} --cluster=${cluster} --user=${user} || return 1
        kubectl config use ${CI_PROJECT_NAME} || return 1
      fi
    fi
}

deployment_name() {
    echo "${CI_PROJECT_NAME}-local"
}

check_required_environment() {
  local required_env="${1}"

  for reqvar in $required_env
  do
    if [ -z "${!reqvar}" ]
    then
      echo "missing ENVIRONMENT ${reqvar}!" >&2
      return 1
    fi
  done
}

helm_chart_dir() {
  echo "${PWD}/helm-chart/${HELM_CHART_NAME}"
}

check_project_helm_chart_dir() {
  if [ ! -d $(helm_chart_dir) ]
  then
    echo "this project does not have a helm-chart directory!" >&2
    return 1
  fi
}

check_required_project_environment() {
  check_required_environment "HELM_CHART_NAME CI_PROJECT_NAME"
}

check_required_deployment_environment() {
  check_required_environment "CI_ENVIRONMENT_NAME CI_ENVIRONMENT_URL" || return 1
}

check_image() {
  docker images | grep "${CI_PROJECT_NAME}_server.*latest"
  if [ $? -gt 0 ]
  then
    echo "docker-compose build server or docker build -t ${CI_PROJECT_NAME}_server ." >&2
    return 1
  fi
}

check_helm_deployment_requirements() {
  if ! is_mac
  then
    if [ -z "${KUBECONFIG}" ]
    then
      echo "SET KUBECONFIG to the kubernetes config file for your k8s" >&2
      return 1
    fi
  fi
  check_required_project_environment || return 1
  check_image || return 1
  check_project_helm_chart_dir || return 1
  check_required_deployment_environment || return 1
  check_required_database_environment || return 1
  check_required_container_env || return 1
  check_application_specific_environment || return 1
}

check_required_container_env() {
  [ -z "${CONTAINER_ENV}" ] && return
  check_required_environment "${CONTAINER_ENV}"
}

check_application_specific_environment() {
  [ -z "${APPLICATION_SPECIFIC_ENVIRONMENT}" ] && return
  check_required_environment "${APPLICATION_SPECIFIC_ENVIRONMENT}"
}

check_required_database_environment() {
    has_missing=0
    check_required_environment "DB__USER DB__PASSWORD DB__NAME DB__SERVER"
}

image_settings() {
    echo "--set-string image.repository=${CI_PROJECT_NAME}_server,image.tag=latest"
}

deployment_environment_settings() {
  echo "--set-string environment=${CI_ENVIRONMENT_NAME} --set-string url=${CI_ENVIRONMENT_URL} --set-string isLocalKube=1"
}

container_env_settings() {
  settings=""
  if [ -n "${CONTAINER_ENV}" ]
  then
    settings="$(env | grep -E "^(${CONTAINER_ENV// /|})=" | sed 's/^/containerEnv./; s/ /\\ /g; s/,/\\,/g' | paste -sd',' -)"
  fi
  echo "${settings:+--set-string ${settings}}"
}

user_settings() {
  if is_mac
  then
    echo "--set-string localKubeDirectory=${PWD} --set podSecurityContext.runAsUser=1000"
  else
    echo "--set-string localKubeDirectory=${PWD} --set podSecurityContext.runAsUser=$(id -u)"
  fi
}

local_gems() {
  if [ -n "${LOCAL_GEMS}" ]
  then
    set_string=''
    for path in ${LOCAL_GEMS}
    do
      key=$(basename $path | tr '_' '-')
      set_string="${set_string} --set-string localGemDirectories.${key}=${path}"
    done
    echo "${set_string}"
  else
    echo ''
  fi
}

application_specific_settings() {
  settings=""
  if [ -n "${APPLICATION_SPECIFIC_ENVIRONMENT}" ]
  then
    settings="$(env | grep -E "^(${APPLICATION_SPECIFIC_ENVIRONMENT// /|})=" | sed 's/ /\\ /g; s/,/\\,/g' | sed -e 's/^\([^=]*\)*__/\1./' | paste -sd',' -)"
  fi
  echo "${settings:+--set-string ${settings}}"
}

database_environment_settings() {
  echo "--set-string db.name=${DB__NAME},db.user=${DB__USER},db.password=${DB__PASSWORD},db.server=${DB__SERVER},${DB__IMAGE:+db.image=${DB__IMAGE}}"
}

lint_template() {
  helm lint $(helm_chart_dir)  \
$(image_settings) \
$(deployment_environment_settings) \
$(database_environment_settings) \
$(container_env_settings) \
$(application_specific_settings) \
$(user_settings) \
$(local_gems)
}

print_template() {
  helm template $(deployment_name) $(helm_chart_dir) \
$(image_settings) \
$(deployment_environment_settings) \
$(database_environment_settings) \
$(container_env_settings) \
$(application_specific_settings) \
$(user_settings) \
$(local_gems)
}

deploy_template() {
  helm upgrade $(deployment_name) $(helm_chart_dir) \
--namespace ${CI_PROJECT_NAME} --reset-values --debug --wait --install \
$(image_settings) \
$(deployment_environment_settings) \
$(database_environment_settings) \
$(container_env_settings) \
$(application_specific_settings) \
$(user_settings) \
$(local_gems)
}

configure_domain() {
  bin/local-kube/configure_local_development_domain.sh || return 1
  echo "visit ${CI_ENVIRONMENT_URL} in your browser" >&2
}

run_main() {
  check_helm_deployment_requirements || return 1
  set_kubectl_context || return 1

  lint_template
  if [ $? -gt 0 ]
  then
    echo "linting failed" >&2
    return 1
  fi

  print_template
  if [ $? -gt 0 ]
  then
    echo "could not print template" >&2
    return 1
  fi

  deploy_template
  if [ $? -gt 0 ]
  then
    echo "could not deploy template" >&2
    return 1
  fi

  configure_domain || return 1
  return
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  if is_mac
  then
    eval $(minikube docker-env)
  fi

  if [ ! -f "local-kube.env" ]
  then
    echo "missing local-kube.env" >&2
    exit 1
  fi


  source local-kube.env
  export $(grep -v '#' local-kube.env | cut -d= -f1)

  export LOCAL_GEMS=$@
  run_main
  if [ $? -gt 0 ]
  then
    exit 1
  fi
fi

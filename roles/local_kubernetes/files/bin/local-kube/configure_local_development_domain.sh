#!/bin/bash

is_mac() {
  [ "$(uname -s)" == "Darwin" ]
}

if [ "$EUID" -ne 0 ]
then
  echo "this will run with sudo" >&2
fi

if [ ! -f "local-kube.env" ]
then
  echo "missing local-kube.env" >&2
  exit 1
fi

source local-kube.env
export $(grep -v '#' local-kube.env | cut -d= -f1)

export domain="${CI_ENVIRONMENT_URL##*/}"
if is_mac
then
  export dns_ip='localhost'
else
  export dns_ip=$(kubectl get -n kube-system services/traefik -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
fi

if ! grep "${dns_ip}.*${domain}" /etc/hosts > /dev/null 2>&1
then
  if grep "${domain}" /etc/hosts
  then
    echo "removing old hosts entry" >&2
    sudo -E su root -c 'sed -i.local.bak "/${domain}/d" /etc/hosts'
    sudo rm /etc/hosts.local.bak
  fi
  echo "adding ${domain} to /etc/hosts"
  sudo -E su root -c 'echo "${dns_ip} ${domain}" >> /etc/hosts'
fi

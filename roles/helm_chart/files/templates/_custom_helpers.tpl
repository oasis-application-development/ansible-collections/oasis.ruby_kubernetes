{{/* vim: set filetype=mustache: */}}
{{/*
Create base 64 encoded image pull secret
*/}}
{{- define "imagePullSecret" }}
{{- $root := required "registry.root is required!" .Values.registry.root -}}
{{- $registryUser := required "registry.secret.username is required for imagePullSecret" .Values.registry.secret.username -}}
{{- $registryPassword := required "registry.secret.password is required for imagePullSecret" .Values.registry.secret.password -}}
{{- printf "{\"auths\": {\"%s\": {\"auth\": \"%s\"}}}" $root (printf "%s:%s" $registryUser $registryPassword | b64enc) | b64enc -}}
{{- end }}
